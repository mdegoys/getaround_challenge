class Car
  attr_reader :price_per_day, :price_per_km

  def initialize(car_attributes)
    @price_per_day = car_attributes["price_per_day"]
    @price_per_km = car_attributes["price_per_km"]
  end
end
