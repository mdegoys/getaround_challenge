require_relative('../rental_price')

describe RentalPrice do
  describe '#calculate' do
    let(:rental) { instance_double("Rental") }
    let(:car) { instance_double("Car") }
    let(:duration) { 5 }
    let(:price_per_day) { 1 }
    let(:distance) { 10 }
    let(:price_per_km) { 2 }
    subject { described_class.new(rental, car).calculate }

    before do
      allow(rental).to receive(:duration).and_return(duration)
      allow(rental).to receive(:distance).and_return(distance)
      allow(car).to receive(:price_per_day).and_return(price_per_day)
      allow(car).to receive(:price_per_km).and_return(price_per_km)
    end

    context 'duration is 0' do
      let(:duration) { 0 }

      it 'calculates the price according to distance only' do
        expect(subject).to eq(20)
      end
    end

    context 'distance is 0' do
      let(:distance) { 0 }

      it 'calculates the price according to duration only' do
        expect(subject).to eq(5)
      end
    end

    context 'distance and duration are not positive' do
      it 'calculates the price according to duration and distance' do
        expect(subject).to eq(25)
      end
    end
  end
end
