class Rental
  require 'date'
  attr_reader :distance

  def initialize(rental_attributes)
    @distance = rental_attributes["distance"]
    @start_date = rental_attributes["start_date"]
    @end_date = rental_attributes["end_date"]
  end

  def date(string)
    Date.strptime(string,'%Y-%m-%d')
  end

  def duration
    (date(@end_date) - date(@start_date)).to_i + 1
  end
end
