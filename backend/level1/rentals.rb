require_relative('./json_parser')
require_relative('./car')
require_relative('./rental')
require_relative('./rental_price')

class Rentals
  def initialize(file)
    @json_parsed = JsonParser.new(file)
  end

  def prices
    { rentals:
      @json_parsed.rentals_attributes.map do |rental_attributes|
        id = rental_attributes["id"]
        car_id = rental_attributes["car_id"]
        car_attributes = @json_parsed.car_attributes(car_id)
        rental = Rental.new(rental_attributes)
        car = Car.new(car_attributes)
        price = RentalPrice.new(rental, car).calculate
        { id: id, price: price }
      end
    }
  end
end
