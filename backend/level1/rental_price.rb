class RentalPrice
  def initialize(rental, car)
    @rental = rental
    @car = car
  end

  def calculate
    @car.price_per_day * @rental.duration + @rental.distance * @car.price_per_km
  end
end
