require_relative('../rental_price')

describe RentalPrice do
  describe '#total' do
    let(:rental) { instance_double("Rental") }
    let(:car) { instance_double("Car") }
    let(:duration) { 1 }
    let(:price_per_day) { 50 }
    let(:distance) { 10 }
    let(:price_per_km) { 20 }
    subject { described_class.new(rental, car).total }

    before do
      allow(rental).to receive(:duration).and_return(duration)
      allow(rental).to receive(:distance).and_return(distance)
      allow(car).to receive(:price_per_day).and_return(price_per_day)
      allow(car).to receive(:price_per_km).and_return(price_per_km)
    end

    context 'distance is 0' do
      let(:distance) { 0 }

      it 'calculates the price according to duration only' do
        expect(subject).to eq(50)
      end

      context 'the duration is less than one day' do
        it 'gives no discount' do
          expect(subject).to eq(50)
        end
      end

      context 'duration is at more than one day' do
        let(:duration) { 2 }

        it 'decreases the price by 10% after one day' do
          expect(subject).to eq(95)
        end
      end

      context 'duration is more than 4 days' do
        let(:duration) { 5 }
        it 'decreases the price by 30% after 4 days' do
          expect(subject).to eq(220)
        end
      end

      context 'duration is more than 10 days' do
        let(:duration) { 11 }
        it 'decreases the price by 30% after 4 days' do
          expect(subject).to eq(420)
        end
      end
    end

    context 'distance and duration are positive' do
      it 'calculates the price according to duration and distance' do
        expect(subject).to eq(250)
      end
    end
  end
end
