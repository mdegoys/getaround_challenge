require_relative('../rental')
require('json')

describe Rental do
  describe '#date' do
    let(:rental_json) { JSON.parse({ id: 1, distance: 10, start_date: start_date_string, end_date: end_date_string }.to_json) }
    let(:start_date_string) { '2022-12-10' }
    let(:end_date_string) { '2022-12-12' }

    subject { described_class.new(rental_json).date(start_date_string) }

    it 'converts the string date into a Date object' do
      expect(subject.class).to eq(Date)
    end

    it 'parses the year, month and day correctly' do
      expect(subject.year).to eq(2022)
      expect(subject.month).to eq(12)
      expect(subject.day).to eq(10)
    end
  end

  describe '#duration' do
    let(:rental_json) { JSON.parse({ id: 1, distance: 10, start_date: start_date_string, end_date: end_date_string }.to_json) }
    let(:start_date_string) { '2022-12-10' }
    let(:end_date_string) { '2022-12-12' }

    subject { described_class.new(rental_json).duration }

    it 'subtract end date with start date, adding one day' do
      expect(subject).to eq(3)
    end
  end
end
