class Rental
  require 'date'
  attr_reader :car_id, :distance

  def initialize(rental_attributes)
    @car_id =  rental_attributes["car_id"]
    @distance = rental_attributes["distance"]
    @start_date = rental_attributes["start_date"]
    @end_date = rental_attributes["end_date"]
  end

  def date(string)
    Date.strptime(string,'%Y-%m-%d')
  end

  def duration
    (date(@end_date) - date(@start_date)).to_i + 1
  end
end
