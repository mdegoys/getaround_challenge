class Commission
  COMMISSION_PERCENTAGE = 30
  INSURANCE_PERCENTAGE = 50
  ASSISTANCE_FEE = 100

  def initialize(price, duration)
    @price = price
    @duration = duration
  end

  def split
    commission_total = ((@price * COMMISSION_PERCENTAGE).to_f/100).to_i
    insurance_fee = ((commission_total * INSURANCE_PERCENTAGE).to_f/100).to_i
    assistance_fee = ASSISTANCE_FEE * @duration
    drivy_fee = commission_total - insurance_fee - assistance_fee
    { insurance_fee: insurance_fee, assistance_fee: assistance_fee, drivy_fee: drivy_fee }
  end
end
