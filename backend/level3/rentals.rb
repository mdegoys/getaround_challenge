require_relative('./json_parser')
require_relative('./car')
require_relative('./rental')
require_relative('./rental_price')
require_relative('./commission')

class Rentals
  def initialize(file)
    @json_parsed = JsonParser.new(file)
  end

  def output
    { rentals:
      @json_parsed.rentals_attributes.map do |rental_attributes|
        id = rental_attributes["id"]
        rental = Rental.new(rental_attributes)
        price = price(rental)
        commission_split = commission_split(price, rental)
        { id: id, price: price, commission: commission_split }
      end
    }
  end

  private

  def price(rental)
    car_id = rental.car_id
    car_attributes = @json_parsed.car_attributes(car_id)
    car = Car.new(car_attributes)
    price = RentalPrice.new(rental, car).total
  end

  def commission_split(price, rental)
    duration = rental.duration
    Commission.new(price, duration).split
  end
end
