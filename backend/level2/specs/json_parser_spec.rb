require_relative('../json_parser')
require('json')

describe JsonParser do
  describe '#cars_attributes' do
    subject { described_class.new(file).cars_attributes }
    let(:file) { './level1/data/input.json' }

    it 'returns an array' do
      expect(subject.class).to eq(Array)
    end

    it 'returns an array of car attributes' do
      hash_body = subject[0]
      expect(hash_body.keys).to match_array(["id", "price_per_day", "price_per_km"])
    end
  end

  describe '#car_attributes' do
    subject { described_class.new(file).car_attributes(car_id) }
    let(:file) { './level1/data/input.json' }
    let(:car_id) { 1 }

    it 'returns car attributes' do
      hash_body = subject
      expect(hash_body.keys).to match_array(["id", "price_per_day", "price_per_km"])
    end
  end

  describe '#rentals_attributes' do
    subject { described_class.new(file).rentals_attributes }
    let(:file) { './level1/data/input.json' }

    it 'returns an array' do
      expect(subject.class).to eq(Array)
    end

    it 'returns an array of rental attributes' do
      hash_body = subject[0]
      expect(hash_body.keys).to match_array(["id", "car_id", "distance", "start_date", "end_date"])
    end
  end
end
