require_relative('../rentals')
require('json')

describe Rentals do
  describe '#prices' do

    subject { described_class.new(file).prices }
    let(:file) { './level1/data/input.json' }

    it 'returns a hash containing rentals' do
       hash_body = subject
       expect(hash_body.keys).to match_array([:rentals])
    end

    it 'returns rentals containing ids and prices' do
       hash_body = subject[:rentals][0]
       expect(hash_body.keys).to match_array([:id, :price])
    end
  end
end
