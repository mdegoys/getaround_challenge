class Money
  ACTORS =
  [
    { name: 'driver', type: 'debit' },
    { name: 'owner', type: 'credit' },
    { name: 'insurance', type: 'credit' },
    { name: 'assistance', type: 'credit' },
    { name: 'drivy', type: 'credit' },
  ]

  def initialize(commission)
    @commission_split = commission.split
    @commission_basis = commission.basis
    @commission_percentage = commission.percentage
  end

  def actions
    ACTORS.map do |actor|
      actor_fee_symbol = "#{actor[:name]}_fee".to_sym
      actor_amount = @commission_split[actor_fee_symbol] || self.send(actor_fee_symbol)
      { who: actor[:name], type: actor[:type], amount: actor_amount }
    end
  end

  private

  def driver_fee
    @commission_basis
  end

  def owner_fee
    (@commission_basis * (100 - @commission_percentage).to_f/100).to_i
  end
end
