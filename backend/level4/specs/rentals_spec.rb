require_relative('../rentals')
require('json')

describe Rentals do
  describe '#output' do

    subject { described_class.new(file).output }
    let(:file) { './level1/data/input.json' }

    it 'returns a hash containing rentals' do
       hash_body = subject
       expect(hash_body.keys).to match_array(%i[rentals])
    end

    it 'returns rentals containing ids, prices and commissions' do
       hash_body = subject[:rentals][0]
       expect(hash_body.keys).to match_array(%i[id price commission])
    end
  end
end
