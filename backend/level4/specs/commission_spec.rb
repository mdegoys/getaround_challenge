require_relative('../commission')

describe Commission do
  describe '#split' do
    let(:price) { 10000 }
    let(:duration) { 10 }

    subject { described_class.new(price, duration).split }

    it 'returns a hash containing insurance, assistance and drivy fees' do
      expect(subject.keys).to match_array(%i[insurance_fee assistance_fee drivy_fee])
    end

    it 'takes into account the commission and insurance percentages for the insurance fee' do
      expect(subject[:insurance_fee]).to eq(1500)
    end

    it 'takes into account the duration and daily assistance fee for the assistance fee' do
      expect(subject[:assistance_fee]).to eq(1000)
    end

    it 'leaves the rest of the commission to the drivy fee' do
      expect(subject[:drivy_fee]).to eq(500)
    end
  end
end
