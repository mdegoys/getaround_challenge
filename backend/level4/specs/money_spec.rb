require_relative('../money')
require_relative('../commission')

describe Money do
  describe '#actions' do
    let(:commission) { instance_double("Commission") }
    let(:percentage) { Commission::COMMISSION_PERCENTAGE }
    let(:basis) { 10_000 }
    let(:insurance_fee) { 1_500 }
    let(:assistance_fee) { 500 }
    let(:drivy_fee) { 1500 }
    let(:split) { { insurance_fee: insurance_fee, assistance_fee: assistance_fee, drivy_fee: drivy_fee } }

    before do
      allow(commission).to receive(:percentage).and_return(percentage)
      allow(commission).to receive(:basis).and_return(basis)
      allow(commission).to receive(:split).and_return(split)
    end

    subject { described_class.new(commission).actions }

    it 'returns an array of hashes containing who, type and amount keys' do
      expect(subject.class).to eq(Array)
      expect(subject[0].keys).to match_array(%i[who type amount])
    end

    it 'returns an array containing values for every actor' do
      Money::ACTORS.each do |actor|
        expect(subject.find { |action| action[:who] == actor[:name] }).not_to eq(nil)
      end
    end

    it 'returns the whole commission basis as a debit for the driver' do
      owner_action = subject.find { |action| action[:who] == 'driver' }
      expect(owner_action[:amount]).to eq(basis)
      expect(owner_action[:type]).to eq('debit')
    end

    it 'returns the whole commission basis minus the commission as a credit for the owner' do
      owner_action = subject.find { |action| action[:who] == 'owner' }
      expect(owner_action[:amount]).to eq(7000)
      expect(owner_action[:type]).to eq('credit')
    end

    it 'returns the insurance fee as a credit for the insurance' do
      owner_action = subject.find { |action| action[:who] == 'insurance' }
      expect(owner_action[:amount]).to eq(insurance_fee)
      expect(owner_action[:type]).to eq('credit')
    end

    it 'returns the assistance fee as a credit for the assistance' do
      owner_action = subject.find { |action| action[:who] == 'assistance' }
      expect(owner_action[:amount]).to eq(assistance_fee)
      expect(owner_action[:type]).to eq('credit')
    end

    it 'returns the drivy fee as a credit for drivy' do
      owner_action = subject.find { |action| action[:who] == 'drivy' }
      expect(owner_action[:amount]).to eq(drivy_fee)
      expect(owner_action[:type]).to eq('credit')
    end
  end
end
