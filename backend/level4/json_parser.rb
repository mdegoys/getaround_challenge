class JsonParser
  require 'json'
  attr_reader :cars, :rentals

  def initialize(file)
    input_json = File.read(file)
    @input_hash = JSON.parse(input_json)
  end

  def cars_attributes
    @input_hash["cars"]
  end

  def car_attributes(id)
    cars_attributes.find { |car| car["id"] == id }
  end

  def rentals_attributes
    @input_hash["rentals"]
  end
end
