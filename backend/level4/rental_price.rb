class RentalPrice
  DISCOUNT_PER_DURATION = [
    { duration: 1, amount: 0 },
    { duration: 3, amount: 10 },
    { duration: 6, amount: 30 },
    { duration: Float::INFINITY, amount: 50 }
  ]

  def initialize(rental, car)
    @rental = rental
    @car = car
  end

  def total
    duration_price + distance_price
  end

  private

  def duration_price
    duration_charged = 0
    price = 0
    DISCOUNT_PER_DURATION.each do |discount|
      duration_left = @rental.duration - duration_charged
      discount_percentage = (100 - discount[:amount]).to_f/100
      discounted_price = (@car.price_per_day * discount_percentage).to_i
      discounted_duration = [duration_left, discount[:duration]].min
      price += discounted_price * discounted_duration
      duration_charged += discounted_duration
    end
    price
  end

  def distance_price
    @rental.distance * @car.price_per_km
  end
end
